require('dotenv').config();
const Mastodon = require('mastodon-api');
const fs = require('fs');

console.log("Mastodon Bot starting...");

const M = new Mastodon({
    client_key: process.env.CLIENT_KEY,
    client_secret: process.env.CLIENT_SECRET,
    access_token: process.env.ACCESS_TOKEN,
    timeout_ms: 60 * 1000,
    api_url: process.env.API_URL,
});

const listener = M.stream('streaming/user')
listener.on('error', err => console.log(err))

listener.on('message', msg => {

    if (msg.event === 'notification') {
        const acct = msg.data.account.acct;

        if (msg.data.type === 'follow') {
            toot(`@${acct} Benvingut/da!`);

        } else if (msg.data.type === 'mention') {

            const id = msg.data.status.id;

            text_toot = cercaInsult();
            toot(`@${acct} ${text_toot}`);

            M.post(`statuses/${id}/favourite`, (error, data) => {
                if (error) console.error(error);
                else console.log(`Favorited ${id} ${data.id}`);
            });
        }
    }
});

function cercaInsult() {

    num_random = numAleatori();
    let fitxer_insults = fs.readFileSync('insults.json', 'UTF-8');
    let fitxer_insults_parse = JSON.parse(fitxer_insults);

    return Object.values(fitxer_insults_parse[num_random]).join('');
}

function numAleatori() {

    return Math.floor(Math.random() * 1715);
}

function toot(content, id) {
    const params = {
        status: content
    }
    if (id) {
        params.in_reply_to_id = id;
    }
    M.post('statuses', params, (error, data) => {
        if (error) {
            console.error(error);
        } else {
            console.log(`ID:${data.id}and timestamp:${data.created_at} `);
            console.log(data.content);
        }
    });
}